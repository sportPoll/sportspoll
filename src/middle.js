import  React from "react";


class Middle extends React.Component{

    render() {
        return (<div>
            <div className='sport_event'>

                <table id="event_items">
                    <tbody>
                    <tr>
                        <th>
                            Away
                        </th>
                        <th>
                            Draw
                        </th>
                        <th>
                            Away
                        </th>


                        <th>
                            Name
                        </th>


                        <th>
                            Group
                        </th>
                        <th>
                            Sport
                        </th>
                        <th>
                            State
                        </th>
                        <th>
                            Country
                        </th>
                        <th>
                            Created date
                        </th>
                        <th>
                            ID
                        </th>
                    </tr>
                    </tbody>



                </table>
            </div>
        </div>);
    }

   static randomEngine(){
        return  (Math.random() * 20.5).toFixed(2);
    }

    // The Event table data function query for Event data.
    static eventTableData(val){




        let eventItems=document.querySelector("#event_items");
        eventItems.innerHTML+=`<tr>
          <td title="click to select Away"  id="away" class="team">
             ${val.awayName +"-"+ this.randomEngine()}
          </td>
           <td title="click to select home" id="draw" class="team">
            
             ${ this.randomEngine()}
          </td>
            <td  title="click to select home"  id="home" class="team">
                   ${val.homeName +"-"+ this.randomEngine()}
              </td>
          <td  class = "team" >
             ${val.group}
         
              </td>
              
             
               
               <td class = "name" >
               ${val.name}
              </td>
               <td class = "sport_type" >
               ${val.sport}
              </td>
               <td class = "state" >
               ${val.state}
              </td>
               <td class = "home_name" >
               ${val.country}
              </td>
               <td class = "create_date" >
              ${val.createdAt}
              </td>
              <td class = "id" >
              <div></div>
              
                 ${val.id}
              </td>
             
          </tr>

      
          `;
        let team=document.querySelectorAll(".team");
        team.forEach(function (el) {
            el.addEventListener("click",function (el) {
                el.target.style.cssText="background:mediumslateblue;color:white";

            })
        })

    }

}
export default Middle;