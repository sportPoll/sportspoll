import test from "./test-assignment";
import React from 'react';

import fr from "./fr.png";
import eng from "./eng.jpg";
import sv from "./sv.jpg";
import Footer from "./footer";
import Header from "./header"
import Middle from "./middle"


class SportsPoll extends React.Component{

    constructor(){

        super();
        this.state={
            away:"",
            home:"",
        }
    }

    /**
     *
     * @returns {*}
     */
    // Execution point for sport poll view;
    // The render is inherit from React Component for rendering Element
    render(){
        return (<div>
            <Header/>
            <Middle/>
            <Footer/>
        </div>);
    }


    // The view function is function that display event object



    //Store data in local storage
    //The execute database engine function query and prepare statement for the eventTableData function;

    executeDBEngine(){
        let eventList="";

        
        if (typeof(Storage) !== "undefined") {
            // Store
            localStorage.setItem("polls",JSON.stringify(test));

            //get storage
            eventList=JSON.parse(localStorage.getItem("polls"));
        }


        let shuffle=Math.floor(Math.random() * Math.floor(5));
        let flag=document.querySelector("#flag");
        

        this.voteEngine();

        for(let val of eventList){


            if(val.country==="SWEDEN"){
           

                flag.style.cssText="width: 1%;float: left;margin-top: -6px;padding: 20px;background: url("+ sv+") no-repeat;background-size: 60px;";
            }else   if(val.country==="FRANCE"){


                flag.style.cssText="width: 1%;float: left;margin-top: -6px;padding: 20px;background: url("+ fr+") no-repeat;background-size: 60px;";
            }else   if(val.country==="ENGLAND"){


                flag.style.cssText="width: 1%;float: left;margin-top: -6px;padding: 20px;background: url("+ eng+") no-repeat;background-size: 60px;";
            }
        
             
            if(shuffle.valueOf()===0 && val.sport==="FOOTBALL"){
                Middle.eventTableData(val);
                return false;


            }else  if(shuffle.valueOf()===1 && val.sport==="SNOOKER"){
                Middle.eventTableData(val);
                return false;


            } else  if(shuffle.valueOf()===2 && val.sport==="HANDBALL"){
                Middle.eventTableData(val);
                return false;


            }else if(shuffle.valueOf()===3 && val.sport==="ICE_HOCKEY"){
                Middle.eventTableData(val);
                return false;



            }else if(shuffle.valueOf()===4 && val.sport==="TENNIS"){
                Middle.eventTableData(val);
                return false;

            }





        }

        


    }
  
    //The vote engine determines the poll winner or draw
    voteEngine(){
     
        let vote=document.querySelector("#vote");
      
        vote.addEventListener("click",function (e) {
            
       
            window.location.reload();
            e.stopImmediatePropagation();
            e.preventDefault();
        });





    }
    componentDidMount() {
        this.executeDBEngine();


    }


}
export default SportsPoll
